<?php


namespace User890104;


use Exception;

/**
 * Class QueryClient
 * @package User890104
 */
class QueryClient
{
    /**
     * @var QuerySocket
     */
    protected $socket;

    const REQUEST_A2S_INFO = 0x54;
    const RESPONSE_A2S_INFO = 0x49;

    const REQUEST_A2S_PLAYER = 0x55;
    const RESPONSE_A2S_PLAYER = 0x44;

    const REQUEST_A2S_RULES = 0x56;
    const RESPONSE_A2S_RULES = 0x45;

    /**
     * QueryClient constructor.
     * @param string $host
     * @param int $port
     * @param int $connectTimeout
     * @param int $readTimeout
     * @throws Exception
     */
    public function __construct(string $host, int $port = 27015, int $connectTimeout = 5, int $readTimeout = 5)
    {
        $this->socket = new QuerySocket($host, $port, $connectTimeout);
        $this->socket->setTimeout($readTimeout);
    }

    /**
     * @return array
     * @throws Exception
     */
    public function info(): array
    {
        $data = $this->socket->communicate(
            static::REQUEST_A2S_INFO,
            static::RESPONSE_A2S_INFO,
            'Source Engine Query' . chr(0)
        );

        $reader = new ByteStreamReader($data);

        $info = [
            'Protocol'		=> $reader->readUnsignedChar(),
            'Name'			=> $reader->readString(),
            'Map'			=> $reader->readString(),
            'Folder'		=> $reader->readString(),
            'Game'			=> $reader->readString(),
            'AppID'			=> $reader->readUnsignedShortLE(),
            'Players'		=> $reader->readUnsignedChar(),
            'MaxPlayers'	=> $reader->readUnsignedChar(),
            'Bots'			=> $reader->readUnsignedChar(),
            'ServerType'	=> $reader->readAsciiLetter(),
            'Environment'	=> $reader->readAsciiLetter(),
            'Visibility'	=> $reader->readUnsignedChar(),
            'VAC'			=> $reader->readUnsignedChar(),
            'Version'		=> $reader->readString(),
        ];

        if ($reader->getRemainingLength() === 0) {
            return $info;
        }

        $edf = $reader->readUnsignedChar();

        if ($edf & 0x80) {
            $info['Port'] = $reader->readUnsignedShortLE();
        }

        if ($edf & 0x10) {
            $info['SteamID'] = $reader->readUnsignedLongLE();
        }

        if ($edf & 0x40) {
            $info['SourceTVPort'] = $reader->readUnsignedShortLE();
            $info['SourceTVName'] = $reader->readString();
        }

        if ($edf & 0x20) {
            $info['Keywords'] = $reader->readString();
        }

        if ($edf & 0x01) {
            $info['GameID'] = $reader->readUnsignedLongLE();
        }

        return $info;
    }

    /**
     * @return array
     * @throws Exception
     */
    public function players(): array
    {
        $data = $this->socket->communicateWithChallenge(
            static::REQUEST_A2S_PLAYER,
            static::RESPONSE_A2S_PLAYER
        );

        $reader = new ByteStreamReader($data);

        $numPlayers = $reader->readUnsignedChar();
        $players = [];

        for ($i = 0; $i < $numPlayers; ++$i) {
            $players[] = [
                'Index' => $reader->readUnsignedChar(),
                'Name' => $reader->readString(),
                'Score' => $reader->readInt(),
                'Duration' => $reader->readFloat(),
            ];
        }

        return $players;
    }

    /**
     * @return array
     * @throws Exception
     */
    public function rules(): array
    {
        $data = $this->socket->communicateWithChallenge(
            static::REQUEST_A2S_RULES,
            static::RESPONSE_A2S_RULES
        );

        $reader = new ByteStreamReader($data);

        $numRules = $reader->readUnsignedShortLE();
        $rules = [];

        for ($i = 0; $i < $numRules; ++$i) {
            $rules[$reader->readString()] = $reader->readString();
        }

        return $rules;
    }
}
