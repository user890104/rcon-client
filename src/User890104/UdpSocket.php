<?php


namespace User890104;


class UdpSocket extends Socket
{
    public function __construct(string $host, int $port, int $connectTimeout = 5)
    {
        parent::__construct('udp://' . $host . ':' . $port, $connectTimeout);
    }
}
