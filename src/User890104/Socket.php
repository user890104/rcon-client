<?php

namespace User890104;

use Exception;

/**
 * Class Socket
 * @package User890104
 */
class Socket
{
    /**
     * @var false|resource
     */
    protected $stream;

    /**
     * TCPConnection constructor.
     * @param string $uri
     * @param int $connectTimeout
     * @throws Exception
     */
    public function __construct(string $uri, int $connectTimeout = 5)
    {
        $stream = stream_socket_client($uri, $errno, $errstr, $connectTimeout);

        if ($stream === false) {
            throw new Exception('Failed to connect: ' . $errstr, $errno);
        }

        $this->stream = $stream;
    }

    /**
     * @throws Exception
     */
    public function __destruct()
    {
        if ($this->stream === null) {
            return;
        }

        $result = fclose($this->stream);

        if ($result === false) {
            throw new Exception('Failed to close the stream');
        }

        $this->stream = null;
    }

    /**
     * @param int $timeout
     * @throws Exception
     */
    public function setTimeout(int $timeout): void
    {
        $result = stream_set_timeout($this->stream, $timeout);

        if ($result === false) {
            throw  new Exception('Unable to set stream timeout');
        }
    }

    /**
     * @param string $data
     * @return int
     * @throws Exception
     */
    public function transmitUnsafe(string $data): int
    {
        $result = fwrite($this->stream, $data);

        if ($result === false) {
            throw new Exception('Failed writing to stream');
        }

        return $result;
    }

    /**
     * @param string $data
     * @throws Exception
     */
    public function transmit(string $data): void
    {
        $length = strlen($data);
        $written = $this->transmitUnsafe($data);

        if ($written < $length) {
            throw new Exception('Unable to write all bytes (written: ' . $written . ', expected: ' . $length . ', missing: ' . ($length - $written) . ')');
        }

        if ($written > $length) {
            throw new Exception('Written more bytes than expected (written: ' . $written . ', expected: ' . $length . ', extra: ' . ($written - $length) . ')');
        }
    }

    /**
     * @param int $length
     * @return string
     * @throws Exception
     */
    public function receiveUnsafe(int $length): string
    {
        $data = fread($this->stream, $length);

        if ($data === false) {
            throw new Exception('Failed reading from stream');
        }

        return $data;
    }

    /**
     * @param int $length
     * @return string
     * @throws Exception
     */
    public function receive(int $length): string
    {
        $data = $this->receiveUnsafe($length);
        $read = strlen($data);

        if ($read < $length) {
            throw new Exception('Unable to read all bytes (read: ' . $read . ', expected: ' . $length . ', missing: ' . ($length - $read) . ')');
        }

        if ($read > $length) {
            throw new Exception('Read more bytes than expected (read: ' . $read . ', expected: ' . $length . ', extra: ' . ($read - $length) . ')');
        }

        return $data;
    }
}
