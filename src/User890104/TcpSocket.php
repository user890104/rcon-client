<?php


namespace User890104;


class TcpSocket extends Socket
{
    public function __construct(string $host, int $port, int $connectTimeout = 5)
    {
        parent::__construct('tcp://' . $host . ':' . $port, $connectTimeout);
    }
}
