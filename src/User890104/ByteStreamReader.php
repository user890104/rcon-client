<?php

namespace User890104;

use Exception;

/**
 * Class ByteStreamReader
 * @package User890104
 */
class ByteStreamReader
{
    /**
     * @var string
     */
    private $data;
    /**
     * @var int
     */
    private $length;
    /**
     * @var int
     */
    private $offset = 0;

    /**
     * ByteStreamReader constructor.
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
        $this->length = strlen($data);
    }

    /**
     * @return int
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * @return int
     */
    public function getOffset()
    {
        return $this->offset;
    }

    /**
     * @return int
     */
    public function getRemainingLength()
    {
        return $this->length - $this->offset;
    }

    /**
     * @return bool|string
     */
    public function getRemainingData()
    {
        return substr($this->data, $this->offset);
    }

    /**
     * @return mixed
     */
    public function getAllData()
    {
        return $this->data;
    }

    public function reset()
    {
        $this->offset = 0;
    }

    /**
     * @return bool|string
     */
    public function readRemainingData()
    {
        $data = $this->getRemainingData();
        $this->offset = $this->length;
        return $data;
    }

    // 8 bit

    /**
     * @return mixed
     * @throws Exception
     */
    public function readChar()
    {
        return $this->read('c', 1);
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public function readUnsignedChar()
    {
        return $this->read('C', 1);
    }

    // 16 bit

    /**
     * @return mixed
     * @throws Exception
     */
    public function readShort()
    {
        return $this->read('s', 2);
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public function readUnsignedShort()
    {
        return $this->read('S', 2);
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public function readUnsignedShortBE()
    {
        return $this->read('n', 2);
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public function readUnsignedShortLE()
    {
        return $this->read('v', 2);
    }

    // 32 bit

    /**
     * @return mixed
     * @throws Exception
     */
    public function readInt()
    {
        return $this->read('l', 4);
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public function readUnsignedInt()
    {
        return $this->read('L', 4);
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public function readUnsignedIntBE()
    {
        return $this->read('N', 4);
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public function readUnsignedIntLE()
    {
        return $this->read('V', 4);
    }

    // 64 bit

    /**
     * @return mixed
     * @throws Exception
     */
    public function readLong()
    {
        return $this->read('q', 8);
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public function readUnsignedLong()
    {
        return $this->read('Q', 8);
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public function readUnsignedLongBE()
    {
        return $this->read('J', 8);
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public function readUnsignedLongLE()
    {
        return $this->read('P', 8);
    }

    // Float

    /**
     * @return mixed
     * @throws Exception
     */
    public function readFloat()
    {
        return $this->read('f', 4);
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public function readDouble()
    {
        return $this->read('d', 8);
    }

    // Text

    /**
     * @return mixed
     */
    public function readAsciiLetter()
    {
        $data = $this->data[$this->getOffset()];
        $this->increaseOffset(1);

        return $data;
    }

    /**
     * @return bool|string
     * @throws Exception
     */
    public function readString()
    {
        $pos = strpos($this->data, chr(0), $this->offset);

        if ($pos === false) {
            throw new Exception('No end marker found');
        }

        $data = substr($this->data, $this->offset, $pos - $this->offset);
        $this->offset = $pos + 1;

        return $data;
    }

    /**
     * @param $count
     */
    private function increaseOffset($count)
    {
        $this->offset += $count;
    }

    /**
     * @param $format
     * @param $length
     * @return mixed
     * @throws Exception
     */
    private function read($format, $length)
    {
        if ($length > $this->getRemainingLength()) {
            throw new Exception('Not enough data');
        }

        $data = unpack($format, substr($this->data, $this->offset, $length));

        if ($data === false) {
            throw new Exception('Failed to decode data');
        }

        $this->increaseOffset($length);

        return reset($data);
    }
}
