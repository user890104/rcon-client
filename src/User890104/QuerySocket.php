<?php


namespace User890104;


use Exception;

/**
 * Class QuerySocket
 * @package User890104
 */
class QuerySocket extends UdpSocket
{
    protected $challenge;
    protected $serverType;

    protected const MAX_PACKET_SIZE = 1400;
    protected const PACKET_HEADER_NOT_SPLIT = -1;
    protected const PACKET_HEADER_SPLIT = -2;

    protected const CHALLENGE_REQUEST_PAYLOAD = "\xFF\xFF\xFF\xFF";
    protected const RESPONSE_CHALLENGE = 0x41;

    /**
     * @param int $requestCode
     * @param int $responseCode
     * @param string $payload
     * @return string
     * @throws Exception
     */
    public function communicate(int $requestCode, int $responseCode, string $payload = ''): string
    {
        $response = $this->transceive(pack('ca*', $requestCode, $payload));

        if (strlen($response) < 1) {
            throw new Exception('Too short response');
        }

        if (ord($response[0]) !== $responseCode) {
            throw new Exception('Wrong response code received, ');
        }

        return substr($response, 1);
    }

    /**
     * @param int $requestCode
     * @param int $responseCode
     * @return string
     * @throws Exception
     */
    public function communicateWithChallenge(int $requestCode, int $responseCode): string
    {
        if ($this->challenge === null) {
            $this->challenge = $this->communicate(
                $requestCode,
                static::RESPONSE_CHALLENGE,
                static::CHALLENGE_REQUEST_PAYLOAD
            );
        }

        return $this->communicate($requestCode, $responseCode, $this->challenge);
    }

    /**
     * @param string $data
     * @return string
     * @throws Exception
     */
    protected function transceive(string $data): string
    {
        $this->transmit(pack('la*', static::PACKET_HEADER_NOT_SPLIT, $data));
        $response = $this->receiveInternal($header, $id, $compressed, $packetsCount, $packetNumber, $packetMaxSize);

        if ($header === static::PACKET_HEADER_NOT_SPLIT) {
            return $this->decompressIfNeeded($response, $compressed);
        }

        $buffer = [];
        $correctId = $id;
        $correctCompressed = $compressed;
        $correctPacketsCount = $packetsCount;
        $remainingPackets = $packetsCount;

        while (true) {
            if (array_key_exists($packetNumber, $buffer)) {
                throw new Exception('Duplicate packet received!');
            }

            $buffer[$packetNumber] = $response;
            $remainingPackets--;

            if ($remainingPackets <= 0) {
                break;
            }

            do {
                $response = $this->receiveInternal($header, $id, $compressed, $packetsCount, $packetNumber, $packetMaxSize);
            }
            while (
                $header !== static::PACKET_HEADER_SPLIT ||
                $id !== $correctId ||
                $compressed !== $correctCompressed ||
                $packetsCount !== $correctPacketsCount
            );
        }

        $response = $this->decompressIfNeeded(implode('', $buffer), $correctCompressed);
        $response = $this->extractHeader($response, $header);

        if ($header !== static::PACKET_HEADER_NOT_SPLIT) {
            throw new Exception('Invalid combined packet');
        }

        return $response;
    }

    /**
     * @param int $header
     * @param int|null $id
     * @param bool|null $compressed
     * @param int|null $packetsCount
     * @param int|null $packetNumber
     * @param int|null $packetMaxSize
     * @return string
     * @throws Exception
     */
    protected function receiveInternal(?int &$header, ?int &$id, ?bool &$compressed, ?int &$packetsCount, ?int &$packetNumber, ?int &$packetMaxSize): string
    {
        $response = $this->receiveUnsafe(static::MAX_PACKET_SIZE);
        $reader = new ByteStreamReader($response);

        $header = $reader->readInt();

        if ($header === static::PACKET_HEADER_NOT_SPLIT) {
            $id = 0;
            $compressed = false;
            $packetsCount = 1;
            $packetNumber = 0;
            $packetMaxSize = static::MAX_PACKET_SIZE;

            return $reader->readRemainingData();
        }

        if ($header === static::PACKET_HEADER_SPLIT) {
            $idAndCompressed = $reader->readInt();
            $totalOrPacketNumber = $reader->readUnsignedChar();

            if ($this->serverType === null) {
                $this->detectServerType($reader->getRemainingData());
            }

            switch ($this->serverType) {
                case 'goldsrc':
                    $id = $idAndCompressed;
                    $compressed = false;
                    $packetNumber = $totalOrPacketNumber >> 4;
                    $packetsCount = $totalOrPacketNumber & 0x0F;
                    $packetMaxSize = static::MAX_PACKET_SIZE;
                    break;
                case 'source':
                    $id = $idAndCompressed & 0x7FFFFFFF;
                    $compressed = $idAndCompressed >> 31 > 0;
                    $packetNumber = $reader->readUnsignedChar();
                    $packetsCount = $totalOrPacketNumber;
                    $packetMaxSize = $reader->readUnsignedShortBE();
                    break;
                default:
                    throw new Exception('Unknown server type ' . $this->serverType);
            }

            return $reader->readRemainingData();
        }

        throw new Exception('Invalid response header');
    }

    /**
     * @param string $data
     * @param int|null $header
     * @return string
     * @throws Exception
     */
    protected function extractHeader(string $data, ?int &$header): string
    {
        $len = strlen($data);

        if ($len === 0) {
            throw new Exception('Read timeout');
        }

        if ($len < 4) {
            throw new Exception('Response too short, length=' . $len);
        }

        /** @noinspection SpellCheckingInspection */
        $packet = unpack('lheader', $data);
        $header = $packet['header'];

        return substr($data, 4);
    }

    /**
     * @param string $data
     * @throws Exception
     */
    protected function detectServerType(string $data): void
    {
        $len = strlen($data);

        if ($len < 4) {
            throw new Exception('Data too short, length=' . $len);
        }

        /** @noinspection SpellCheckingInspection */
        $packet = unpack('lheader', $data);

        if ($packet['header'] === static::PACKET_HEADER_NOT_SPLIT) {
            $this->serverType = 'goldsrc';
        }
        else {
            $this->serverType = 'source';
        }
    }

    /**
     * @param string $data
     * @param bool $compressed
     * @return string
     */
    protected function decompressIfNeeded(string $data, bool $compressed): string
    {
        if (!$compressed) {
            return $data;
        }

        return bzdecompress($data);
    }
}
