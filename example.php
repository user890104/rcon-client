<?php
require __DIR__ . '/vendor/autoload.php';

use User890104\QueryClient;
use User890104\RconClient;

$rcon = new RconClient('example.com', '123456');

echo $rcon->exec('stats');
echo $rcon->exec('status');
echo $rcon->exec('users');
//echo $rcon->exec('maps *');

$query = new QueryClient('example.com');
var_dump($query->info());
//var_dump($query->players());
//var_dump($query->rules());
